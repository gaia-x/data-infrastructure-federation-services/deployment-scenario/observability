package com.dawex.datatransactionaudit;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.testcontainers.context.ImportTestcontainers;

@SpringBootTest
@ImportTestcontainers(DataTransactionAuditContainers.class)
class ApplicationTests {

	@Test
	@SuppressWarnings("java:S2699")
	void contextLoads() {
	}

}
