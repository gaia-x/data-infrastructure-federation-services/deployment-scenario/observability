package com.dawex.datatransactionaudit.common;

import com.dawex.datatransactionaudit.DataTransactionAuditContainers;
import com.dawex.datatransactionaudit.dto.AccessEventRequestDto;
import com.dawex.datatransactionaudit.dto.AuditRequestDto;
import com.dawex.datatransactionaudit.dto.ContractEventRequestDto;
import com.dawex.datatransactionaudit.dto.OnboardingEventRequestDto;
import com.dawex.datatransactionaudit.dto.VerifiableCredentialDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.testcontainers.context.ImportTestcontainers;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ImportTestcontainers(DataTransactionAuditContainers.class)
@AutoConfigureMockMvc
class DataTransactionAuditControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Nested
	class StoreContractEvent {
		@Test
		void shouldStoreContractEvent() throws Exception {
			final String acquirerId = "https://example.org/acquirer/b73ceb2c-f328-4909-8283-a233e64e9854";
			final String dataProducerId = "https://example.org/dataProducer/deb4604e-4286-40a6-83be-895a100a4628";
			final String dataProductId = "https://example.org/dataProduct/dda46e99-4512-4ff7-9d3a-45595a366606";
			final String dataContractId = "https://example.org/dataContract/24f55524-f6a6-4d4c-ac0f-b1480cdec80d";
			final ContractEventRequestDto request = new ContractEventRequestDto(
					new VerifiableCredentialDto(acquirerId),
					new VerifiableCredentialDto(dataProducerId),
					new VerifiableCredentialDto(dataProductId),
					new VerifiableCredentialDto(dataContractId));

			mockMvc.perform(post("/contract")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON)
							.content(objectMapper.writeValueAsBytes(request)))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.dataProductId").value(dataProductId))
					.andExpect(jsonPath("$.dataContractId").value(dataContractId));
		}
	}

	@Nested
	class StoreDataProductAccessEvent {
		@Test
		void shouldStoreDataProductAccessEvent() throws Exception {
			final String acquirerId = "https://example.org/acquirer/b73ceb2c-f328-4909-8283-a233e64e9854";
			final String dataProducerId = "https://example.org/dataProducer/deb4604e-4286-40a6-83be-895a100a4628";
			final String dataProductId = "https://example.org/dataProduct/dda46e99-4512-4ff7-9d3a-45595a366606";
			final String dataContractId = "https://example.org/dataContract/24f55524-f6a6-4d4c-ac0f-b1480cdec80d";
			final AccessEventRequestDto request = new AccessEventRequestDto(
					new VerifiableCredentialDto(acquirerId),
					new VerifiableCredentialDto(dataProducerId),
					new VerifiableCredentialDto(dataProductId),
					new VerifiableCredentialDto(dataContractId));

			mockMvc.perform(post("/access")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON)
							.content(objectMapper.writeValueAsBytes(request)))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.dataProductId").value(dataProductId))
					.andExpect(jsonPath("$.dataContractId").value(dataContractId));
		}
	}

	@Nested
	class StoreParticipantOnboardingEvent {
		@Test
		void shouldStoreParticipantOnboardingEvent() throws Exception {
			final String participantId = "https://example.org/participant/3d190339-d342-4cee-a884-68d2018b032c";
			final OnboardingEventRequestDto request = new OnboardingEventRequestDto(
					new VerifiableCredentialDto(participantId));

			mockMvc.perform(post("/onboard")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON)
							.content(objectMapper.writeValueAsBytes(request)))
					.andExpect(status().isOk())
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.participantId").value(participantId));
		}
	}

	@Nested
	class SearchForEvents {
		@Test
		void shouldSearchForEvents() throws Exception {
			final String acquirerId = "https://example.org/participant/257948d8-6770-452d-ae52-5a85a1728d5e";
			final String dataProducerId = "https://example.org/participant/0c22aefc-b9b2-4cf3-8d08-aecbd6a45287";
			final String dataProductId = "https://example.org/dataProduct/38ca0861-4401-48cb-b04e-ae87df9b600b";
			final String dataContractId = "https://example.org/dataContract/7e763049-944e-4fe8-b651-3f2a12c43c55";
			final AuditRequestDto request = new AuditRequestDto(acquirerId, dataProducerId, dataProductId, dataContractId);

			mockMvc.perform(post("/audit")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON)
							.content(objectMapper.writeValueAsBytes(request)))
					.andExpect(status().isOk())

					.andExpect(jsonPath("$.content", hasSize(4)))

					.andExpect(jsonPath("$.content[0].id").value("834aaf4c-3a30-420b-929e-479da1048a20"))
					.andExpect(jsonPath("$.content[0].timestamp").value("2023-01-06T14:16:09Z"))
					.andExpect(jsonPath("$.content[0].type").value("ONBOARDING"))
					.andExpect(jsonPath("$.content[0].acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.content[0].dataProducerId").isEmpty())
					.andExpect(jsonPath("$.content[0].dataProductId").isEmpty())
					.andExpect(jsonPath("$.content[0].dataContractId").isEmpty())

					.andExpect(jsonPath("$.content[1].id").value("39c16816-b714-425e-999a-7972871acff7"))
					.andExpect(jsonPath("$.content[1].timestamp").value("2023-01-06T21:46:22Z"))
					.andExpect(jsonPath("$.content[1].type").value("ONBOARDING"))
					.andExpect(jsonPath("$.content[1].acquirerId").isEmpty())
					.andExpect(jsonPath("$.content[1].dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.content[1].dataProductId").isEmpty())
					.andExpect(jsonPath("$.content[1].dataContractId").isEmpty())

					.andExpect(jsonPath("$.content[2].id").value("b4b4d9aa-6561-41c6-94a3-125a7345a80f"))
					.andExpect(jsonPath("$.content[2].timestamp").value("2023-02-10T12:16:16Z"))
					.andExpect(jsonPath("$.content[2].type").value("CONTRACT"))
					.andExpect(jsonPath("$.content[2].acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.content[2].dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.content[2].dataProductId").value(dataProductId))
					.andExpect(jsonPath("$.content[2].dataContractId").value(dataContractId))

					.andExpect(jsonPath("$.content[3].id").value("d9f04113-7a08-4425-91f0-a06a77494e0b"))
					.andExpect(jsonPath("$.content[3].timestamp").value("2023-03-10T02:14:08Z"))
					.andExpect(jsonPath("$.content[3].type").value("ACCESS"))
					.andExpect(jsonPath("$.content[3].acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.content[3].dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.content[3].dataProductId").value(dataProductId))
					.andExpect(jsonPath("$.content[3].dataContractId").value(dataContractId));
		}
	}
}