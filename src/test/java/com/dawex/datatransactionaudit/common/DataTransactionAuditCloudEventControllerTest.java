package com.dawex.datatransactionaudit.common;

import com.dawex.datatransactionaudit.DataTransactionAuditContainers;
import com.dawex.datatransactionaudit.dto.AccessEventRequestDto;
import com.dawex.datatransactionaudit.dto.ContractEventRequestDto;
import com.dawex.datatransactionaudit.dto.OnboardingEventRequestDto;
import com.dawex.datatransactionaudit.dto.VerifiableCredentialDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cloudevents.CloudEvent;
import io.cloudevents.core.builder.CloudEventBuilder;
import io.cloudevents.core.data.PojoCloudEventData;
import io.cloudevents.core.format.ContentType;
import io.cloudevents.core.provider.EventFormatProvider;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.testcontainers.context.ImportTestcontainers;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URI;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ImportTestcontainers(DataTransactionAuditContainers.class)
@AutoConfigureMockMvc
class DataTransactionAuditCloudEventControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;

	@Nested
	class ConsumeCloudEventInBinaryContentMode {
		@Test
		void shouldStoreContractEvent() throws Exception {
			final String acquirerId = "https://example.org/acquirer/b73ceb2c-f328-4909-8283-a233e64e9854";
			final String dataProducerId = "https://example.org/dataProducer/deb4604e-4286-40a6-83be-895a100a4628";
			final String dataProductId = "https://example.org/dataProduct/dda46e99-4512-4ff7-9d3a-45595a366606";
			final String dataContractId = "https://example.org/dataContract/24f55524-f6a6-4d4c-ac0f-b1480cdec80d";
			final ContractEventRequestDto request = new ContractEventRequestDto(
					new VerifiableCredentialDto(acquirerId),
					new VerifiableCredentialDto(dataProducerId),
					new VerifiableCredentialDto(dataProductId),
					new VerifiableCredentialDto(dataContractId));

			mockMvc.perform(post("/ce/binary")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON)
							.header("ce-id", "A234-1234-1234")
							.header("ce-source", "/test/DataTransactionAuditCloudEventControllerTest")
							.header("ce-specversion", "1.0")
							.header("ce-type", "data-transaction-audit.contract")
							.content(objectMapper.writeValueAsBytes(request)))
					.andExpect(status().isOk())
					.andExpect(header().exists("ce-id"))
					.andExpect(header().exists("ce-source"))
					.andExpect(header().string("ce-specversion", "1.0"))
					.andExpect(header().string("ce-type", "data-transaction-audit.stored"))
					.andExpect(header().string("ce-subject", "data-transaction-audit.contract"))
					.andExpect(header().exists("ce-time"))
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.dataProductId").value(dataProductId))
					.andExpect(jsonPath("$.dataContractId").value(dataContractId));
		}

		@Test
		void shouldStoreDataProductAccessEvent() throws Exception {
			final String acquirerId = "https://example.org/acquirer/b73ceb2c-f328-4909-8283-a233e64e9854";
			final String dataProducerId = "https://example.org/dataProducer/deb4604e-4286-40a6-83be-895a100a4628";
			final String dataProductId = "https://example.org/dataProduct/dda46e99-4512-4ff7-9d3a-45595a366606";
			final String dataContractId = "https://example.org/dataContract/24f55524-f6a6-4d4c-ac0f-b1480cdec80d";
			final AccessEventRequestDto request = new AccessEventRequestDto(
					new VerifiableCredentialDto(acquirerId),
					new VerifiableCredentialDto(dataProducerId),
					new VerifiableCredentialDto(dataProductId),
					new VerifiableCredentialDto(dataContractId));

			mockMvc.perform(post("/ce/binary")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON)
							.header("ce-id", "A234-1234-1234")
							.header("ce-source", "/test/DataTransactionAuditCloudEventControllerTest")
							.header("ce-specversion", "1.0")
							.header("ce-type", "data-transaction-audit.access")
							.content(objectMapper.writeValueAsBytes(request)))
					.andExpect(status().isOk())
					.andExpect(header().exists("ce-id"))
					.andExpect(header().exists("ce-source"))
					.andExpect(header().string("ce-specversion", "1.0"))
					.andExpect(header().string("ce-type", "data-transaction-audit.stored"))
					.andExpect(header().string("ce-subject", "data-transaction-audit.access"))
					.andExpect(header().exists("ce-time"))
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.dataProductId").value(dataProductId))
					.andExpect(jsonPath("$.dataContractId").value(dataContractId));
		}

		@Test
		void shouldStoreParticipantOnboardingEvent() throws Exception {
			final String participantId = "https://example.org/participant/3d190339-d342-4cee-a884-68d2018b032c";
			final OnboardingEventRequestDto request = new OnboardingEventRequestDto(
					new VerifiableCredentialDto(participantId));

			mockMvc.perform(post("/ce/binary")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(MediaType.APPLICATION_JSON)
							.header("ce-id", "A234-1234-1234")
							.header("ce-source", "/test/DataTransactionAuditCloudEventControllerTest")
							.header("ce-specversion", "1.0")
							.header("ce-type", "data-transaction-audit.onboard")
							.content(objectMapper.writeValueAsBytes(request)))
					.andExpect(status().isOk())
					.andExpect(header().exists("ce-id"))
					.andExpect(header().exists("ce-source"))
					.andExpect(header().string("ce-specversion", "1.0"))
					.andExpect(header().string("ce-type", "data-transaction-audit.stored"))
					.andExpect(header().string("ce-subject", "data-transaction-audit.onboard"))
					.andExpect(header().exists("ce-time"))
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.participantId").value(participantId));
		}
	}

	@Nested
	class ConsumeCloudEventInStructuredContentMode {
		@Test
		void shouldStoreContractEvent() throws Exception {
			final String acquirerId = "https://example.org/acquirer/b73ceb2c-f328-4909-8283-a233e64e9854";
			final String dataProducerId = "https://example.org/dataProducer/deb4604e-4286-40a6-83be-895a100a4628";
			final String dataProductId = "https://example.org/dataProduct/dda46e99-4512-4ff7-9d3a-45595a366606";
			final String dataContractId = "https://example.org/dataContract/24f55524-f6a6-4d4c-ac0f-b1480cdec80d";
			final ContractEventRequestDto request = new ContractEventRequestDto(
					new VerifiableCredentialDto(acquirerId),
					new VerifiableCredentialDto(dataProducerId),
					new VerifiableCredentialDto(dataProductId),
					new VerifiableCredentialDto(dataContractId));
			final CloudEvent cloudEvent = CloudEventBuilder.v1()
					.withId("A234-1234-1234")
					.withSource(URI.create("/test/DataTransactionAuditCloudEventControllerTest"))
					.withType("data-transaction-audit.contract")
					.withDataContentType(MediaType.APPLICATION_JSON_VALUE)
					.withData(PojoCloudEventData.wrap(request, objectMapper::writeValueAsBytes))
					.build();

			mockMvc.perform(post("/ce/structured")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(DataTransactionAuditCloudEventConstants.MediaType.APPLICATION_CLOUDEVENTS_JSON)
							.content(EventFormatProvider.getInstance().resolveFormat(ContentType.JSON).serialize(cloudEvent)))
					.andExpect(status().isOk())
					.andExpect(header().exists("ce-id"))
					.andExpect(header().exists("ce-source"))
					.andExpect(header().string("ce-specversion", "1.0"))
					.andExpect(header().string("ce-type", "data-transaction-audit.stored"))
					.andExpect(header().string("ce-subject", "data-transaction-audit.contract"))
					.andExpect(header().exists("ce-time"))
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.dataProductId").value(dataProductId))
					.andExpect(jsonPath("$.dataContractId").value(dataContractId));
		}

		@Test
		void shouldStoreDataProductAccessEvent() throws Exception {
			final String acquirerId = "https://example.org/acquirer/b73ceb2c-f328-4909-8283-a233e64e9854";
			final String dataProducerId = "https://example.org/dataProducer/deb4604e-4286-40a6-83be-895a100a4628";
			final String dataProductId = "https://example.org/dataProduct/dda46e99-4512-4ff7-9d3a-45595a366606";
			final String dataContractId = "https://example.org/dataContract/24f55524-f6a6-4d4c-ac0f-b1480cdec80d";
			final AccessEventRequestDto request = new AccessEventRequestDto(
					new VerifiableCredentialDto(acquirerId),
					new VerifiableCredentialDto(dataProducerId),
					new VerifiableCredentialDto(dataProductId),
					new VerifiableCredentialDto(dataContractId));
			final CloudEvent cloudEvent = CloudEventBuilder.v1()
					.withId("A234-1234-1234")
					.withSource(URI.create("/test/DataTransactionAuditCloudEventControllerTest"))
					.withType("data-transaction-audit.access")
					.withDataContentType(MediaType.APPLICATION_JSON_VALUE)
					.withData(PojoCloudEventData.wrap(request, objectMapper::writeValueAsBytes))
					.build();

			mockMvc.perform(post("/ce/structured")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(DataTransactionAuditCloudEventConstants.MediaType.APPLICATION_CLOUDEVENTS_JSON)
							.content(EventFormatProvider.getInstance().resolveFormat(ContentType.JSON).serialize(cloudEvent)))
					.andExpect(status().isOk())
					.andExpect(header().exists("ce-id"))
					.andExpect(header().exists("ce-source"))
					.andExpect(header().string("ce-specversion", "1.0"))
					.andExpect(header().string("ce-type", "data-transaction-audit.stored"))
					.andExpect(header().string("ce-subject", "data-transaction-audit.access"))
					.andExpect(header().exists("ce-time"))
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.acquirerId").value(acquirerId))
					.andExpect(jsonPath("$.dataProducerId").value(dataProducerId))
					.andExpect(jsonPath("$.dataProductId").value(dataProductId))
					.andExpect(jsonPath("$.dataContractId").value(dataContractId));
		}

		@Test
		void shouldStoreParticipantOnboardingEvent() throws Exception {
			final String participantId = "https://example.org/participant/3d190339-d342-4cee-a884-68d2018b032c";
			final OnboardingEventRequestDto request = new OnboardingEventRequestDto(
					new VerifiableCredentialDto(participantId));
			final CloudEvent cloudEvent = CloudEventBuilder.v1()
					.withId("A234-1234-1234")
					.withSource(URI.create("/test/DataTransactionAuditCloudEventControllerTest"))
					.withType("data-transaction-audit.onboard")
					.withDataContentType(MediaType.APPLICATION_JSON_VALUE)
					.withData(PojoCloudEventData.wrap(request, objectMapper::writeValueAsBytes))
					.build();

			mockMvc.perform(post("/ce/structured")
							.accept(MediaType.APPLICATION_JSON)
							.contentType(DataTransactionAuditCloudEventConstants.MediaType.APPLICATION_CLOUDEVENTS_JSON)
							.content(EventFormatProvider.getInstance().resolveFormat(ContentType.JSON).serialize(cloudEvent)))
					.andExpect(status().isOk())
					.andExpect(header().exists("ce-id"))
					.andExpect(header().exists("ce-source"))
					.andExpect(header().string("ce-specversion", "1.0"))
					.andExpect(header().string("ce-type", "data-transaction-audit.stored"))
					.andExpect(header().string("ce-subject", "data-transaction-audit.onboard"))
					.andExpect(header().exists("ce-time"))
					.andExpect(jsonPath("$.id").exists())
					.andExpect(jsonPath("$.timestamp").exists())
					.andExpect(jsonPath("$.participantId").value(participantId));
		}
	}
}