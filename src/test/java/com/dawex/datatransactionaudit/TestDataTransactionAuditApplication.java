package com.dawex.datatransactionaudit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.testcontainers.context.ImportTestcontainers;

@TestConfiguration(proxyBeanMethods = false)
@ImportTestcontainers(DataTransactionAuditContainers.class)
public class TestDataTransactionAuditApplication {

	public static void main(String[] args) {
		SpringApplication.from(Application::main).with(TestDataTransactionAuditApplication.class).run(args);
	}

}
