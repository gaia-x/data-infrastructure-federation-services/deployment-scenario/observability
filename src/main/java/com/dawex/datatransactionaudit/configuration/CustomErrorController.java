package com.dawex.datatransactionaudit.configuration;

import jakarta.servlet.http.HttpServletRequest;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;

@Controller
@RequestMapping("${server.error.path:${error.path:/error}}")
public class CustomErrorController extends AbstractErrorController {
	public CustomErrorController(ErrorAttributes errorAttributes) {
		super(errorAttributes, Collections.emptyList());
	}

	@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ProblemDetail error(HttpServletRequest request) {
		final HttpStatus status = getStatus(request);
		return ProblemDetail.forStatus(status);
	}
}
