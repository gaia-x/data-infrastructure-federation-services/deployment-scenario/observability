package com.dawex.datatransactionaudit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class MalformedAuditStorageRequestException extends RuntimeException {

	public MalformedAuditStorageRequestException(String message) {
		super(message);
	}
}
