package com.dawex.datatransactionaudit.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class UnsupportedCloudEventTypeException extends RuntimeException {

	public UnsupportedCloudEventTypeException(String currentType) {
		super("Unsupported event type [%s]".formatted(currentType));
	}
}
