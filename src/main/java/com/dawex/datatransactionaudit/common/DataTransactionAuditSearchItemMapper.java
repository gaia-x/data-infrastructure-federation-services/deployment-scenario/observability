package com.dawex.datatransactionaudit.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DataTransactionAuditSearchItemMapper {

	public static DataTransactionAuditSearchItem toAcquirerItem(DataTransactionAudit audit) {
		return DataTransactionAuditSearchItem.builder()
				.id(audit.getId())
				.timestamp(audit.getTimestamp())
				.type(audit.getType())
				.acquirerId(audit.getParticipantId())
				.build();
	}

	public static DataTransactionAuditSearchItem toDataProducerItem(DataTransactionAudit audit) {
		return DataTransactionAuditSearchItem.builder()
				.id(audit.getId())
				.timestamp(audit.getTimestamp())
				.type(audit.getType())
				.dataProducerId(audit.getParticipantId())
				.build();
	}

	public static DataTransactionAuditSearchItem toContractAuditItem(DataTransactionAudit audit) {
		return DataTransactionAuditSearchItem.builder()
				.id(audit.getId())
				.timestamp(audit.getTimestamp())
				.type(audit.getType())
				.acquirerId(audit.getAcquirerId())
				.dataProducerId(audit.getDataProducerId())
				.dataProductId(audit.getDataProductId())
				.dataContractId(audit.getDataContractId())
				.build();
	}
}
