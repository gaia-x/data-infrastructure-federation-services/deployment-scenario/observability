package com.dawex.datatransactionaudit.common;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
public class DataTransactionAuditSearchItem {

	private UUID id;

	private Instant timestamp;

	private DataTransactionAuditType type;

	private String acquirerId;

	private String dataProducerId;

	private String dataProductId;

	private String dataContractId;
}
