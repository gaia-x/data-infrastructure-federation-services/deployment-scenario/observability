package com.dawex.datatransactionaudit.common;

import java.util.List;

public interface DataTransactionAuditService {
	DataTransactionAudit storeDataTransactionAudit(DataTransactionAudit dataTransactionAudit);

	List<DataTransactionAuditSearchItem> searchForEvents(String acquirerId,
			String dataProducerId,
			String dataProductId,
			String dataContractId);
}
