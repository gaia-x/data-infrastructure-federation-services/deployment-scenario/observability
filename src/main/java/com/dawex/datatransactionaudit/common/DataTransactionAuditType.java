package com.dawex.datatransactionaudit.common;

public enum DataTransactionAuditType {
	ACCESS,
	CONTRACT,
	ONBOARDING
}
