package com.dawex.datatransactionaudit.common;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DataTransactionAuditCloudEventConstants {

	public static final String ACCESS = "data-transaction-audit.access";

	public static final String CONTRACT = "data-transaction-audit.contract";

	public static final String ONBOARD = "data-transaction-audit.onboard";

	public static final String STORED = "data-transaction-audit.stored";

	@NoArgsConstructor(access = AccessLevel.PRIVATE)
	public static final class MediaType {
		public static final String APPLICATION_CLOUDEVENTS_JSON = "application/cloudevents+json";
	}
}
