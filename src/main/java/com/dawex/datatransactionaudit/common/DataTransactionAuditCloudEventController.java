package com.dawex.datatransactionaudit.common;

import com.dawex.datatransactionaudit.dto.AccessEventRequestDto;
import com.dawex.datatransactionaudit.dto.AccessEventResponseDto;
import com.dawex.datatransactionaudit.dto.ContractEventRequestDto;
import com.dawex.datatransactionaudit.dto.ContractEventResponseDto;
import com.dawex.datatransactionaudit.dto.OnboardingEventRequestDto;
import com.dawex.datatransactionaudit.dto.OnboardingEventResponseDto;
import com.dawex.datatransactionaudit.exception.MalformedCloudEventException;
import com.dawex.datatransactionaudit.exception.UnsupportedCloudEventTypeException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cloudevents.CloudEvent;
import io.cloudevents.core.builder.CloudEventBuilder;
import io.cloudevents.core.data.PojoCloudEventData;
import io.cloudevents.jackson.PojoCloudEventDataMapper;
import io.cloudevents.rw.CloudEventRWException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.Validator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.net.URI;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import static io.cloudevents.core.CloudEventUtils.mapData;

@RestController
@RequestMapping(value = "/ce")
@RequiredArgsConstructor
@Slf4j
public class DataTransactionAuditCloudEventController {

	private final DataTransactionAuditService dataTransactionAuditService;

	private final ObjectMapper objectMapper;

	private final Validator validator;

	@Operation(
			operationId = "consumeCloudEventInBinaryContentMode",
			summary = "Consumes a cloud event in binary content mode",
			tags = {"Data Transaction Audit - CloudEvents"},
			parameters = {
					@Parameter(name = "ce-id",
							in = ParameterIn.HEADER,
							description = "Identifies the event.",
							required = true,
							example = "A234-1234-1234"),
					@Parameter(name = "ce-source",
							in = ParameterIn.HEADER,
							description = "Identifies the context in which an event happened.",
							required = true,
							example = "myApp"),
					@Parameter(name = "ce-specversion",
							in = ParameterIn.HEADER,
							description = "The version of the CloudEvents specification which the event uses.",
							required = true,
							example = "\"1.0\""),
					@Parameter(name = "ce-type",
							in = ParameterIn.HEADER,
							description = "Describes the type of event related to the originating occurrence.",
							required = true,
							schema = @Schema(type = "string", allowableValues = {
									"data-transaction-audit.access",
									"data-transaction-audit.contract",
									"data-transaction-audit.onboard"
							}))
			},
			requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
					description = "The event payload.",
					required = true,
					content = {
							@Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(anyOf = {
											AccessEventRequestDto.class,
											ContractEventRequestDto.class,
											OnboardingEventRequestDto.class})
							)
					}),
			responses = {
					@ApiResponse(responseCode = "200",
							description = "Ok",
							headers = {
									@Header(name = "ce-id",
											description = "Identifies the event."),
									@Header(name = "ce-source",
											description = "Identifies the context in which an event happened."),
									@Header(name = "ce-specversion",
											description = "The version of the CloudEvents specification which the event uses."),
									@Header(name = "ce-type",
											description = "Describes the type of event related to the originating occurrence."),
									@Header(name = "ce-subject",
											description = "Describes the subject of the event in the context of the event producer (identified by source)."),
									@Header(name = "ce-time",
											description = "Timestamp of when the occurrence happened. Must adhere to RFC 3339."),
							},
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(anyOf = {
											AccessEventResponseDto.class,
											ContractEventResponseDto.class,
											OnboardingEventResponseDto.class}))),
					@ApiResponse(responseCode = "400",
							description = "Bad request. The request is malformed or contains invalid parameters in the body, URL path, URL query string, or HTTP headers.",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
			}
	)
	@PostMapping(value = "/binary", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CloudEvent consumeCloudEventInBinaryContentMode(@RequestBody CloudEvent event) {
		return consumeCloudEvent(event);
	}

	@Operation(
			operationId = "consumeCloudEventInStructuredContentMode",
			summary = "Consumes a cloud event in structured content mode",
			tags = "Data Transaction Audit - CloudEvents",
			requestBody = @io.swagger.v3.oas.annotations.parameters.RequestBody(
					description = "The cloud event",
					required = true,
					content = @Content(
							mediaType = DataTransactionAuditCloudEventConstants.MediaType.APPLICATION_CLOUDEVENTS_JSON,
							schema = @Schema(implementation = CloudEvent.class),
							examples = {
									@ExampleObject(
											name = "HTTP message for data access",
											value = """
													{
													  "id": "A234-1234-1234",
													  "source": "myApp",
													  "specversion": "1.0",
													  "type": "data-transaction-audit.access",
													  "datacontenttype": "application/json",
													  "data": {
													     "acquirer": {
													       "id": "string"
													     },
													     "dataProducer": {
													       "id": "string"
													     },
													     "dataProduct": {
													       "id": "string"
													     },
													     "dataContract": {
													       "id": "string"
													     }
													  }
													}"""),
									@ExampleObject(
											name = "HTTP message for contract",
											value = """
													{
													  "id": "A234-1234-1234",
													  "source": "myApp",
													  "specversion": "1.0",
													  "type": "data-transaction-audit.contract",
													  "datacontenttype": "application/json",
													  "data": {
													     "acquirer": {
													       "id": "string"
													     },
													     "dataProducer": {
													       "id": "string"
													     },
													     "dataProduct": {
													       "id": "string"
													     },
													     "dataContract": {
													       "id": "string"
													     }
													  }
													}"""),
									@ExampleObject(
											name = "HTTP message for participant onboarding",
											value = """
													{
													  "id": "A234-1234-1234",
													  "source": "myApp",
													  "specversion": "1.0",
													  "type": "data-transaction-audit.onboard",
													  "datacontenttype": "application/json",
													  "data": {
													    "participant": {
													      "id": "string"
													    }
													  }
													}"""),
							})),
			responses = {
					@ApiResponse(responseCode = "200", description = "Ok",
							headers = {
									@Header(name = "ce-id",
											description = "Identifies the event."),
									@Header(name = "ce-source",
											description = "Identifies the context in which an event happened."),
									@Header(name = "ce-specversion",
											description = "The version of the CloudEvents specification which the event uses."),
									@Header(name = "ce-type",
											description = "Describes the type of event related to the originating occurrence."),
									@Header(name = "ce-subject",
											description = "Describes the subject of the event in the context of the event producer (identified by source)."),
									@Header(name = "ce-time",
											description = "Timestamp of when the occurrence happened. Must adhere to RFC 3339."),
							},
							content = @Content(
									mediaType = MediaType.APPLICATION_JSON_VALUE,
									schema = @Schema(anyOf = {
											AccessEventResponseDto.class,
											ContractEventResponseDto.class,
											OnboardingEventResponseDto.class}))),
					@ApiResponse(responseCode = "400",
							description = "Bad request. The request is malformed or contains invalid parameters in the body, URL path, URL query string, or HTTP headers.",
							content = @Content(mediaType = MediaType.APPLICATION_JSON_VALUE))
			}
	)
	@PostMapping(
			value = "/structured",
			consumes = DataTransactionAuditCloudEventConstants.MediaType.APPLICATION_CLOUDEVENTS_JSON,
			produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	public CloudEvent consumeCloudEventInStructuredContentMode(@RequestBody CloudEvent event) {
		return consumeCloudEvent(event);
	}

	private CloudEvent consumeCloudEvent(CloudEvent event) {
		log.info("Consuming cloud event id=[{}] type=[{}]", event.getId(), event.getType());
		final var response = switch (event.getType()) {
			case DataTransactionAuditCloudEventConstants.ACCESS -> consumeAccessEvent(event);
			case DataTransactionAuditCloudEventConstants.CONTRACT -> consumeContractEvent(event);
			case DataTransactionAuditCloudEventConstants.ONBOARD -> consumeOnboardEvent(event);
			default -> throw new UnsupportedCloudEventTypeException(event.getType());
		};
		return toCloudEvent(response);
	}

	private EventResponse<AccessEventResponseDto> consumeAccessEvent(CloudEvent event) {
		final PojoCloudEventData<AccessEventRequestDto> data = getValidatedCloudEventData(event, AccessEventRequestDto.class);
		final DataTransactionAudit audit = dataTransactionAuditService.storeDataTransactionAudit(
				DataTransactionAuditMapper.toDataTransactionAudit(data.getValue()));
		final AccessEventResponseDto response = DataTransactionAuditMapper.toAccessEventResponseDto(audit);

		return new EventResponse<>(response.getId(), event.getType(), response);
	}

	private EventResponse<ContractEventResponseDto> consumeContractEvent(CloudEvent event) {
		final PojoCloudEventData<ContractEventRequestDto> data = getValidatedCloudEventData(event, ContractEventRequestDto.class);
		final DataTransactionAudit audit = dataTransactionAuditService.storeDataTransactionAudit(
				DataTransactionAuditMapper.toDataTransactionAudit(data.getValue()));
		final ContractEventResponseDto response = DataTransactionAuditMapper.toContractEventResponseDto(audit);

		return new EventResponse<>(response.getId(), event.getType(), response);
	}

	private EventResponse<OnboardingEventResponseDto> consumeOnboardEvent(CloudEvent event) {
		final PojoCloudEventData<OnboardingEventRequestDto> data = getValidatedCloudEventData(event,
				OnboardingEventRequestDto.class);
		final DataTransactionAudit audit = dataTransactionAuditService.storeDataTransactionAudit(
				DataTransactionAuditMapper.toDataTransactionAudit(data.getValue()));
		final OnboardingEventResponseDto response = DataTransactionAuditMapper.toOnboardingEventResponseDto(audit);

		return new EventResponse<>(response.getId(), event.getType(), response);
	}

	private <T> PojoCloudEventData<T> getValidatedCloudEventData(CloudEvent event, Class<T> dataClass) {
		final PojoCloudEventData<T> data = mapData(event, PojoCloudEventDataMapper.from(objectMapper, dataClass));
		if (data == null) {
			throw new MalformedCloudEventException(
					"CloudEvent with id [%s] and type [%s] has no data field".formatted(event.getId(), event.getType()));
		}
		final var violations = validator.validate(data.getValue());
		if (!violations.isEmpty()) {
			throw new ConstraintViolationException(violations);
		}
		return data;
	}

	private <T> CloudEvent toCloudEvent(EventResponse<T> response) {
		final String id = response.id().toString();
		return CloudEventBuilder.v1()
				.withId(id)
				.withType(DataTransactionAuditCloudEventConstants.STORED)
				.withSource(URI.create("urn:uuid:" + id))
				.withSubject(response.sourceType())
				.withTime(OffsetDateTime.now(ZoneOffset.UTC))
				.withData(PojoCloudEventData.wrap(response.data(), objectMapper::writeValueAsBytes))
				.build();
	}

	@ExceptionHandler({CloudEventRWException.class, ConstraintViolationException.class, IllegalStateException.class})
	public ProblemDetail handleException(Exception e) {
		log.error("Got exception when processing cloud event: [{}]", e.toString());
		return ProblemDetail.forStatus(HttpStatus.BAD_REQUEST);
	}

	private record EventResponse<T>(UUID id, String sourceType, T data) {
	}
}
