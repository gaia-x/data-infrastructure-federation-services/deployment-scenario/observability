package com.dawex.datatransactionaudit.common;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Entity(name = "data_transaction_audit")
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class DataTransactionAudit {

	@GeneratedValue(strategy = GenerationType.UUID)
	@Id
	@Column(name = "id")
	private UUID id;

	@Column(name = "timestamp")
	private Instant timestamp;

	@Enumerated(EnumType.STRING)
	@Column(name = "type")
	private DataTransactionAuditType type;

	@Column(name = "participant_id")
	private String participantId;

	@Column(name = "acquirer_id")
	private String acquirerId;

	@Column(name = "data_producer_id")
	private String dataProducerId;

	@Column(name = "data_product_id")
	private String dataProductId;

	@Column(name = "data_contract_id")
	private String dataContractId;
}
