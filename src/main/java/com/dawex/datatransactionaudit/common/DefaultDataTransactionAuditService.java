package com.dawex.datatransactionaudit.common;

import com.dawex.datatransactionaudit.exception.MalformedAuditStorageRequestException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class DefaultDataTransactionAuditService implements DataTransactionAuditService {

	private final DataTransactionAuditRepository dataTransactionAuditRepository;

	@Override
	public DataTransactionAudit storeDataTransactionAudit(DataTransactionAudit dataTransactionAudit) {
		if (dataTransactionAudit.getId() != null) {
			throw new MalformedAuditStorageRequestException("Expected a null id for DataTransactionAudit creation");
		}
		return dataTransactionAuditRepository.save(dataTransactionAudit);
	}

	@Override
	public List<DataTransactionAuditSearchItem> searchForEvents(String acquirerId, String dataProducerId, String dataProductId,
			String dataContractId) {
		final Stream<DataTransactionAuditSearchItem> acquirerAuditItems = dataTransactionAuditRepository.findByParticipantId(acquirerId)
				.stream()
				.map(DataTransactionAuditSearchItemMapper::toAcquirerItem);
		final Stream<DataTransactionAuditSearchItem> dataProducerAuditItems = dataTransactionAuditRepository.findByParticipantId(
						dataProducerId).stream()
				.map(DataTransactionAuditSearchItemMapper::toDataProducerItem);
		final Stream<DataTransactionAuditSearchItem> contractAuditItems = dataTransactionAuditRepository.findByAcquirerIdAndDataProducerIdAndDataProductIdAndDataContractId(
						acquirerId, dataProducerId, dataProductId, dataContractId).stream()
				.map(DataTransactionAuditSearchItemMapper::toContractAuditItem);

		return Stream.of(acquirerAuditItems, dataProducerAuditItems, contractAuditItems)
				.flatMap(UnaryOperator.identity())
				.sorted(Comparator.comparing(DataTransactionAuditSearchItem::getTimestamp))
				.toList();
	}
}
