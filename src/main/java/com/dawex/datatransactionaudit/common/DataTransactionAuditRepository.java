package com.dawex.datatransactionaudit.common;

import org.springframework.data.repository.ListCrudRepository;

import java.util.List;
import java.util.UUID;

public interface DataTransactionAuditRepository extends ListCrudRepository<DataTransactionAudit, UUID> {
	List<DataTransactionAudit> findByParticipantId(String participantId);

	List<DataTransactionAudit> findByAcquirerIdAndDataProducerIdAndDataProductIdAndDataContractId(String acquirerId,
			String dataProducerId, String dataProductId, String dataContractId);
}
