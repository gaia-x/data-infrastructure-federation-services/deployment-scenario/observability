package com.dawex.datatransactionaudit.common;

import com.dawex.datatransactionaudit.DataTransactionAuditApi;
import com.dawex.datatransactionaudit.dto.AccessEventRequestDto;
import com.dawex.datatransactionaudit.dto.AccessEventResponseDto;
import com.dawex.datatransactionaudit.dto.AuditRequestDto;
import com.dawex.datatransactionaudit.dto.AuditResponseDto;
import com.dawex.datatransactionaudit.dto.ContractEventRequestDto;
import com.dawex.datatransactionaudit.dto.ContractEventResponseDto;
import com.dawex.datatransactionaudit.dto.OnboardingEventRequestDto;
import com.dawex.datatransactionaudit.dto.OnboardingEventResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class DataTransactionAuditController implements DataTransactionAuditApi {

	private final DataTransactionAuditService dataTransactionAuditService;

	@Override
	public ResponseEntity<AuditResponseDto> searchForEvents(AuditRequestDto auditRequestDto) {
		final List<DataTransactionAuditSearchItem> searchItems = dataTransactionAuditService.searchForEvents(
				auditRequestDto.getAcquirerId(),
				auditRequestDto.getDataProducerId(),
				auditRequestDto.getDataProductId(),
				auditRequestDto.getDataContractId()
		);
		return ResponseEntity.ok(DataTransactionAuditMapper.toAuditResponseDto(searchItems));
	}

	@Override
	public ResponseEntity<ContractEventResponseDto> storeContractEvent(ContractEventRequestDto contractEventRequestDto) {
		final var response = dataTransactionAuditService.storeDataTransactionAudit(
				DataTransactionAuditMapper.toDataTransactionAudit(contractEventRequestDto));
		return ResponseEntity.ok(DataTransactionAuditMapper.toContractEventResponseDto(response));
	}

	@Override
	public ResponseEntity<AccessEventResponseDto> storeDataProductAccessEvent(AccessEventRequestDto accessEventRequestDto) {
		final var response = dataTransactionAuditService.storeDataTransactionAudit(
				DataTransactionAuditMapper.toDataTransactionAudit(accessEventRequestDto));
		return ResponseEntity.ok(DataTransactionAuditMapper.toAccessEventResponseDto(response));
	}

	@Override
	public ResponseEntity<OnboardingEventResponseDto> storeParticipantOnboardingEvent(OnboardingEventRequestDto onboardingEventRequestDto) {
		final var response = dataTransactionAuditService.storeDataTransactionAudit(
				DataTransactionAuditMapper.toDataTransactionAudit(onboardingEventRequestDto));
		return ResponseEntity.ok(DataTransactionAuditMapper.toOnboardingEventResponseDto(response));
	}
}
