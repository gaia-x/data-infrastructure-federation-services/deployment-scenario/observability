package com.dawex.datatransactionaudit.common;

import com.dawex.datatransactionaudit.dto.AccessEventRequestDto;
import com.dawex.datatransactionaudit.dto.AccessEventResponseDto;
import com.dawex.datatransactionaudit.dto.AuditItemDto;
import com.dawex.datatransactionaudit.dto.AuditResponseDto;
import com.dawex.datatransactionaudit.dto.AuditTypeDto;
import com.dawex.datatransactionaudit.dto.ContractEventRequestDto;
import com.dawex.datatransactionaudit.dto.ContractEventResponseDto;
import com.dawex.datatransactionaudit.dto.OnboardingEventRequestDto;
import com.dawex.datatransactionaudit.dto.OnboardingEventResponseDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DataTransactionAuditMapper {

	public static DataTransactionAudit toDataTransactionAudit(ContractEventRequestDto dto) {
		return DataTransactionAudit.builder()
				.timestamp(Instant.now())
				.type(DataTransactionAuditType.CONTRACT)
				.acquirerId(dto.getAcquirer().getId())
				.dataProducerId(dto.getDataProducer().getId())
				.dataProductId(dto.getDataProduct().getId())
				.dataContractId(dto.getDataContract().getId())
				.build();
	}

	public static DataTransactionAudit toDataTransactionAudit(AccessEventRequestDto dto) {
		return DataTransactionAudit.builder()
				.timestamp(Instant.now())
				.type(DataTransactionAuditType.ACCESS)
				.acquirerId(dto.getAcquirer().getId())
				.dataProducerId(dto.getDataProducer().getId())
				.dataProductId(dto.getDataProduct().getId())
				.dataContractId(dto.getDataContract().getId())
				.build();
	}

	public static DataTransactionAudit toDataTransactionAudit(OnboardingEventRequestDto dto) {
		return DataTransactionAudit.builder()
				.timestamp(Instant.now())
				.type(DataTransactionAuditType.ONBOARDING)
				.participantId(dto.getParticipant().getId())
				.build();
	}

	public static ContractEventResponseDto toContractEventResponseDto(DataTransactionAudit audit) {
		final ContractEventResponseDto dto = new ContractEventResponseDto()
				.id(audit.getId())
				.acquirerId(audit.getAcquirerId())
				.dataProducerId(audit.getDataProducerId())
				.dataProductId(audit.getDataProductId())
				.dataContractId(audit.getDataContractId());

		Optional.ofNullable(audit.getTimestamp())
				.map(date -> date.atOffset(ZoneOffset.UTC))
				.ifPresent(dto::timestamp);

		return dto;
	}

	public static AccessEventResponseDto toAccessEventResponseDto(DataTransactionAudit audit) {
		final AccessEventResponseDto dto = new AccessEventResponseDto()
				.id(audit.getId())
				.acquirerId(audit.getAcquirerId())
				.dataProducerId(audit.getDataProducerId())
				.dataProductId(audit.getDataProductId())
				.dataContractId(audit.getDataContractId());

		Optional.ofNullable(audit.getTimestamp())
				.map(date -> date.atOffset(ZoneOffset.UTC))
				.ifPresent(dto::timestamp);

		return dto;
	}

	public static OnboardingEventResponseDto toOnboardingEventResponseDto(DataTransactionAudit audit) {
		final OnboardingEventResponseDto dto = new OnboardingEventResponseDto()
				.id(audit.getId())
				.participantId(audit.getParticipantId());

		Optional.ofNullable(audit.getTimestamp())
				.map(date -> date.atOffset(ZoneOffset.UTC))
				.ifPresent(dto::timestamp);

		return dto;
	}

	public static AuditResponseDto toAuditResponseDto(List<DataTransactionAuditSearchItem> items) {
		return new AuditResponseDto()
				.content(items.stream()
						.map(DataTransactionAuditMapper::toAuditItemDto)
						.toList());
	}

	public static AuditItemDto toAuditItemDto(DataTransactionAuditSearchItem item) {
		final AuditItemDto dto = new AuditItemDto()
				.id(item.getId())
				.type(DataTransactionAuditMapper.toAuditTypeDto(item.getType()))
				.acquirerId(item.getAcquirerId())
				.dataProducerId(item.getDataProducerId())
				.dataProductId(item.getDataProductId())
				.dataContractId(item.getDataContractId());

		Optional.ofNullable(item.getTimestamp())
				.map(date -> date.atOffset(ZoneOffset.UTC))
				.ifPresent(dto::timestamp);

		return dto;
	}

	private static AuditTypeDto toAuditTypeDto(DataTransactionAuditType type) {
		if (type == null) {
			return null;
		}
		return switch (type) {
			case ACCESS -> AuditTypeDto.ACCESS;
			case CONTRACT -> AuditTypeDto.CONTRACT;
			case ONBOARDING -> AuditTypeDto.ONBOARDING;
		};
	}
}
